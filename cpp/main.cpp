#include <iostream>
#include <fstream>
#include <string>
#include <limits>
#include <iterator>
#include <vector>
#include <algorithm> 
#include <boost/program_options.hpp>

namespace po = boost::program_options;
using namespace std;

const string meminfo_path("/proc/meminfo");

unsigned long get_by_token(const string & input_token) {
    std::string token;
    std::ifstream file(meminfo_path);
    while(file >> token) {
        if(token == input_token) {
            unsigned long mem;
            if(file >> mem) {
                return mem;
            } else {
                return 0;       
            }
        }
        file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    return 0;
}

int main (int ac, char* av[]){

    try{
        std::vector<string> allowed_tokens = {
            "ram_total", "ram_taken", "ram_free", "ram_cached", "swap_total", "swap_taken", "swap_free"};
        std::vector<string>::iterator it;
        string token_help = "Give token: ";
        token_help.append(
            [](const std::vector<string> & v){
                string tmp;
                for (auto i = v.begin(); i != v.end(); ++ i){
                    tmp.append(*i);
                    if (*i != v.back()) 
                        tmp.append(", ");
                }
                return tmp; 
            }(allowed_tokens)
        );
        
        po::options_description desc("Allowed options");
        desc.add_options()
            ("help", "Produce this help message.")
            ("debug", "Print intermediate values.")
            ("token", po::value<string>()->default_value("ram_taken"), token_help.c_str())
        ;

        po::variables_map vm;        
        po::store(po::parse_command_line(ac, av, desc), vm);
        po::notify(vm);    

        if (vm.count("help")) {
            cout << desc << "\n";
            return 0;
        }

        if (vm.count("token")) {
            it = find (allowed_tokens.begin(), allowed_tokens.end(), vm["token"].as<string>());
            if (it == allowed_tokens.end()){
                std::cout << "Element not found in allowed parameters\n";
                return 1;
            }
            string token(vm["token"].as<string>());
            if (token == "ram_total"){
                cout << get_by_token("MemTotal:")/1024 << endl;
                return 0;
            }
            if (token == "ram_taken"){
                // cout << (get_by_token("MemTotal:") - get_by_token("MemAvailable:") - get_by_token("Cached:") - get_by_token("Buffers:"))/1024 << endl;
                cout << (get_by_token("MemTotal:") - (get_by_token("MemFree:") + get_by_token("Buffers:") + get_by_token("Cached:")))/1024 << endl;
                if (vm.count("debug"))
                    cout << get_by_token("MemTotal:")/1024 << " - " << get_by_token("MemFree:")/1024 << " - " << get_by_token("Buffers:")/1024 << "-" << get_by_token("Cached:")/1024 << endl;
                return 0;
            }
            if (token == "ram_free"){
                cout << (get_by_token("MemFree:") + get_by_token("Buffers:") + get_by_token("Cached:"))/1024 << endl;
                // cout << (get_by_token("MemFree:") + get_by_token("Cached:"))/1024 << endl;
                return 0;
            }            
            if (token == "ram_cached"){
                cout << (get_by_token("Cached:") + get_by_token("Buffers:"))/1024 << endl;
                return 0;
            }            
            if (token == "swap_total"){
                cout << (get_by_token("SwapTotal:"))/1024 << endl;
                return 0;
            }
            if (token == "swap_free"){
                cout << (get_by_token("SwapFree:"))/1024 << endl;
                return 0;
            }
            if (token == "swap_taken"){
                cout << (get_by_token("SwapTotal:") - get_by_token("SwapFree:"))/1024 << endl;
                return 0;
            }
        } else {
            cout << "Token was not set!\n";
            return 1;
        }
    }
    catch(exception& e) {
        cerr << "error: " << e.what() << "\n";
        return 1;
    }
    catch(...) {
        cerr << "Exception of unknown type!\n";
    }
	return 0;
}