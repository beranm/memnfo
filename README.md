# Memory info binary

Should serve as check for memory usage with cache info, swap and etc. It is more & less in form of Nagios command.

## Build

To build the app you need BOOST library. Everything is beeing taken care of in `Makefile` in `cpp` folder. It is a small app doing close to nothing do not overthing the stuff.

## Deploy

In Ansible forder are two roles

 * `deploy_memnfo` -- deploys the binary containing the memory check
 * `zabbix_memnfo` -- deploys zabbix `userparameter_memnfo.conf`
